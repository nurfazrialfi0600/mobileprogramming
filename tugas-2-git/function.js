function teriak() {
    console.log("Halo Humanika!");
  }
  
  console.log(teriak()) // Menampilkan "Halo Humanika!" di console

  console.log("\n");

  function kalikan(num1, num2) {
    return num1 * num2;
  }

  var num1 = 12;
  var num2 = 4;
  
  var hasilKali = kalikan(num1,num2);
  console.log(hasilKali); // Menampilkan angka 48

  console.log("\n");

  function introduce(name_param, age_param, address_param, hobby_param) {
    console.log("Nama saya " + name_param + ", umur saya " + age_param + ", alamat saya di " + address_param + ", dan saya punya hobby yaitu " + hobby_param);
  }
  
  var name = "Nur Fazri Alfi Maulidiyah";
  var age = 20;
  var address = "Jatiluhur";
  var hobby = "Nonton";
  
  var perkenalan = introduce(name,age,address,hobby);
  console.log(perkenalan); // Menampilkan "Nama saya Nur Fazri Alfi Maulidiyah, umur saya 20 tahun, alamat saya di Jatiluhur, dan saya punya hobby yaitu Nonton!"

  console.log("\n");

  function dataHandling(bioArr){
    for(var i = 0; i < bioArr.length; i++){
      console.log("Nomor ID: " + bioArr[i][0]);
      console.log("Nama Lengkap: " + bioArr[i][1]);
      console.log("TTL: " + bioArr[i][2] + " " + bioArr[i][3]);
      console.log("Hobi: " + bioArr[i][4]);
      console.log("");
    }
  }
  
var input = [
                  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
              ]
  
console.log(dataHandling(input));